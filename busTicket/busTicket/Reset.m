//
//  Reset.m
//  busTicketV2
//
//  Created by  on 9/30/13.
//  Copyright (c) 2013 seneca. All rights reserved.
//

#import "Reset.h"

@interface Reset ()

@end

@implementation Reset

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)Reset_btn:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Important"
        message:@"Are you sure you want to reset the application\nThis can't be undone."
        delegate:self
        cancelButtonTitle:@"Cancel"
        otherButtonTitles:@"Reset", nil];
    [alert show];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    switch (buttonIndex) {
        case 1:
            [self.model reset];
            break;
        default:
            break;
    }
}

@end
