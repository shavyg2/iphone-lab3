//
//  ViewController.h
//  busTicket
//
//  Created by  on 9/24/13.
//  Copyright (c) 2013 seneca. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>{
    NSMutableArray *data;
}

@property (nonatomic, strong) Model *model;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segCity;

@property (weak, nonatomic) IBOutlet UIPickerView *pickPrice;



@property (weak, nonatomic) IBOutlet UILabel *labelPrice;

@property (weak, nonatomic) IBOutlet UILabel *labelQuanity;


- (void)rebuildThePicker;

- (IBAction)segSelectCity:(id)sender;


- (IBAction)btnBuy:(id)sender;


@end
