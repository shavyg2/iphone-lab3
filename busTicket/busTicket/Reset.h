//
//  Reset.h
//  busTicketV2
//
//  Created by  on 9/30/13.
//  Copyright (c) 2013 seneca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reset : UIViewController

@property (nonatomic, strong) Model *model;

- (IBAction)Reset_btn:(id)sender;
@end
