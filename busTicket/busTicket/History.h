//
//  History.h
//  busTicketV2
//
//  Created by  on 9/30/13.
//  Copyright (c) 2013 seneca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface History : UIViewController

@property (nonatomic, strong) Model *model;
@property (weak, nonatomic) IBOutlet UITextView *bought_state;
@end

