//
//  History.m
//  busTicketV2
//
//  Created by  on 9/30/13.
//  Copyright (c) 2013 seneca. All rights reserved.
//

#import "History.h"

@interface History ()

@end

@implementation History

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    _bought_state.text=self.model.bought_history;
    
}

@end
